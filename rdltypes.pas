unit RDLTypes;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

type RDLType = (
     NONE = 0,
     DIRECTIVE = 1, {\[ALNUM]+}
     TEXT = 2,      {[ALNUM]+}
     PROCENT = 3,   {%}
     LSB = 4,       {'['}
     RSB = 5,       {']'}
     EQ = 6,        {'='}
);

implementation

end.

