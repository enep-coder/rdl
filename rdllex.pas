unit RDLLex;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,RDLType,RDLTokens;

type
  RDLLexer = class(TObject)
    const
         SERVICE_CHARS:pchar = "\[]=%";
         SERVICE_TOKENS:array of RDLType = (
                              RDLType.DIRECTIVE,RDLType.LSB,
                              RDLType.RSB,RDLType.EQ,RDLType.PROCENT);
    private
           Input:pchar;
           Tokens:TList;
           PosInInput:integer;
           procedure addToken(tt:RDLType,c:string="");
           function peek(rp:integer):pchar;
           function next:pchar;
    public
          constructor Create(i:pchar);
          function Tokinaze:TList;
          procedure TokinazeDirective;
          procedure TokinazeText;


  end;

implementation


end.

