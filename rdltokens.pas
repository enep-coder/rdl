unit RDLTokens;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,RDLTypes;
type
  RDLToken = object
    private
      TokenType : RDLType;
      Text:string;
    public
       constructor Create(tt:RDLType; t:string);
       destructor Destroy();
       procedure setType(t:RDLToken):
       procedure setText(t:string);
       function getType:RDLToken;
       function getText:string;
  end;


implementation
  constructor RDLToken.Create(tt:RDLType,t:string);
  begin
    self.TokenType := tt;
    self.Text:=t;
    inherited
  end;
  destructor Destroy();
  begin
    self.Text = "";
    inherited
  end;
  procedure setTyoe(t:RDLType);
  begin
    self.TokenType := t;
  end;
  procedure setText(t:string);
  begin
    self.Text := t;
  end;
  function getType:RDLType;
  begin
   result:=self.TokenType;
  end;
  function getText:string;
  begin
   result:=self.Text;
  end;

end.

